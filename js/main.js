(function($) {

	jQuery(document).ready(function($) {

		// Scroll Hash
		if( $(window).width() >= 600 ) {
			$(".content-categories-gift .categories-gift .category-gift a").click(function(e) {
				e.preventDefault();
				var aid = $(this).attr("href").split("#")[1];
				$('html,body').animate({scrollTop: $(".featured-categories-products." + aid + "").offset().top - 150},'slow');
			});
		}

		if( $(window).width() <= 600 ) {
			$(".content-categories-gift .categories-gift .category-gift a").click(function(e) {
				e.preventDefault();
				var aid = $(this).attr("href").split("#")[1];
				$('html,body').animate({scrollTop: $(".featured-categories-products.row-mobile." + aid + "").offset().top - 80},'slow');
			});
		}
		


		if( $(window).width() <= 600 ) {

			$('.categories-gift').slick({
				infinite: true,
				slidesToShow: 3,
				arrows: false,
				dots: false,
				slidesToScroll: 1,
				responsive: [
				  {
					breakpoint: 400,
					settings: {
					  slidesToShow: 3,
					  slidesToScroll: 1,
					  infinite: true,
					  dots: false
					}
				  }
				]
			});

		}

	});

})(jQuery);